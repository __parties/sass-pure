SASS Pure
====

![A set of small, responsive CSS modules that you can use in every web project.](http://f.cl.ly/items/2y0M0E2Q3a2H0z1N1Y19/pure-banner.png "SASS Pure")

A set of small, responsive CSS modules ported to SCSS.

Installation
-------
In some dir in your project:
```shell
$ git clone https://github.com/parties/sass-pure pure
```
Then `@import "./pure/pure.scss";` in whatever main.scss file you have.

Build
-------

If you have the need or desire to change the `pure-` prefix to something else, modify your `gruntfile` accordingly and run:
```shell
$ grunt
```

Before compiling pure.scss with your build tool of choice, you have the option of commenting out any includes that may be redundant or which you know would be unused in your project. The only include which cannot be removed is "pure/variables".

YUI Project License
-------

This software is free to use under the Yahoo! Inc. BSD license.
See the [LICENSE file][] for license text and copyright information.


[LICENSE file]: https://github.com/yui/pure/blob/master/LICENSE.md
