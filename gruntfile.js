module.exports = function(grunt) {
  'use strict';
  // Project configuration
  grunt.initConfig({
    // Metadata
    pkg: grunt.file.readJSON('package.json'),
    replace: {
      pure: {
        src: ['./pure/**/*.scss'],
        dest: './prefixed/',
        replacements: [{
          from: 'pure-',
          to: 'p-'
        }]
      }
    }
  });

  // These plugins provide necessary tasks
  require('load-grunt-tasks')(grunt);

  // Default task
  grunt.registerTask('default', ['replace']);
};
